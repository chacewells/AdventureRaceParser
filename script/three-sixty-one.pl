use v5.18;
use AdventureRace::ThreeSixtyOne;
use Data::Dumper;

my $_361 = AdventureRace::ThreeSixtyOne->new( file => 'data/361.html' );

# print Dumper $_361->_parse_event_page( { url => 'http://361adventures.com/frigid', title => 'Frigid' } );
binmode STDOUT, ':utf8';
print $_361->to_csv;
