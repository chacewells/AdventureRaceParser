use CPAN;

my @depends = qw( Class::CSV Text::CSV_XS File::Slurper JSON LWP::Simple Mojo::DOM Moose MooseX::ClassAttribute WWW::Mechanize YAML namespace::autoclean );

CPAN::Shell->install(@depends);
