use v5.11;
use WWW::Mechanize;
use Mojo::DOM;
use List::MoreUtils qw(uniq);

use constant
SPOOF => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36';

my $mech = WWW::Mechanize->new( agent => SPOOF );
$mech->get('http://goalsara.org/');

my $dom = Mojo::DOM->new($mech->content);

for my $a (reverse uniq grep &future_race, $dom->find('a')->map(sub { $_->{href} })->each) {
    say $a;
}

sub future_race {
    state $nowyear = (localtime)[5] + 1900;
    m@ (\d{4}) -races / [^/]+? / $@x && $1 >= $nowyear;
}
