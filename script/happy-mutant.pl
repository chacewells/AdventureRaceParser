use v5.18;
use AdventureRace::HappyMutant;
binmode STDOUT, ':encoding(UTF-8)';

my $happy_mutant = AdventureRace::HappyMutant->new( url => 'http://www.thehappymutant.com/events/' );

say $happy_mutant->to_csv;
