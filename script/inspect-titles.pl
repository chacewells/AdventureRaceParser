use v5.18;
use Mojo::DOM;
use WWW::Mechanize;
use threads;

use constant SPOOF => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36';

my @urls;
open my $master_list, "<", "data/master-list.csv";
while (<$master_list>) {
    my @token = split /,/;
    push @urls, $token[1] unless not $token[1] or $token[1] !~ /^http/;
}
close $master_list;

my $mech = WWW::Mechanize->new( agent => SPOOF );
my $dom = Mojo::DOM->new;
open my $output, ">", "data/titles.txt";
binmode $output, ":utf8";
for my $url (@urls) {
    my $title = '';
    if (eval { $mech->get($url) }) {
        eval { $dom->parse($mech->content);
        $title = $dom->at('title')->text; };
        $title =~ s/^\s+|\s+$//g;
    }
    say $output "$url\t$title";
    say "$url\t$title";
}
