use v5.18;
use AdventureRace::GoalsARA;
binmode STDOUT, ':encoding(UTF-8)';

my $goals = AdventureRace::GoalsARA->new;

say $goals->to_csv;
