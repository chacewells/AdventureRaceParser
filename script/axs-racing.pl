#!/usr/bin/perl
use v5.18;
use AdventureRace::AXSRacing;

my $axs = AdventureRace::AXSRacing->new( url => AdventureRace::AXSRacing->DEFAULT_URL );

# open AXS_CSV, '>', 'data/axs-racing.csv';
# select AXS_CSV;
print $axs->to_csv;
