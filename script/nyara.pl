use v5.18;
use AdventureRace::NYARA;

my $sourcefile = 'data/nyara-calendar.html';
my $nyara = AdventureRace::NYARA->new(file => $sourcefile);

print $nyara->to_csv;
