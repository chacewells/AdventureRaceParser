#!/usr/bin/env perl
use v5.18;
use Module::Load;

my $module = shift;
$module = "AdventureRace::$module" unless $module =~ /^AdventureRace::/;
load $module;
my $parser = $module->new(url => $module->DEFAULT_URL);

print $parser->to_csv;
