#!/usr/bin/env perl
use v5.10;
use Module::Load;
use IO::Handle;
use Text::CSV_XS qw(csv);
use List::Util qw(any);
use List::MoreUtils qw(uniq);

use constant MONTH => {Jan => 1, Feb => 2, Mar => 3, Apr => 4, May => 5, Jun => 6, Jul => 7, Aug => 8, Sept => 9, Oct => 10, Nov => 11, Dec => 12};

$SIG{CHLD} = 'IGNORE';

my @modules = glob q{lib/AdventureRace/*.pm};

foreach (@modules) {
    s#lib/##;
    s#/#::#g;
    s/\.pm//;

    my $pid = fork;
    warn "couldn't fork: $!", next unless defined $pid;
    
    next if $pid;
    load $_;
    
    my $csv_filename = 'data/' . lc($_ =~ s/^AdventureRace:://r) . '.csv';
    my $parser = eval { $_->new( url => $_->DEFAULT_URL ) }
        or die "Error creating new parser for $_: $!";

    if (my $csv = eval { $parser->to_csv }) {
        open my $out, '>', $csv_filename;
        $out->binmode(':utf8');
        $out->print($csv);
    } else { warn "Something went wrong in $_: $!" }
    exit;
}

1 while wait != -1;

&summarize;

sub summarize {
    state $headers = [qw(month day year title location website)];
    my @result_files = grep !/master/, glob "data/*.csv";
    my @results =
        ( $headers,
            map [ @$_{ @$headers } ],
            sort { $a->{year} <=> $b->{year}
                || MONTH->{$a->{month}} <=> MONTH->{$b->{month}}
                || $a->{day} <=> $b->{day}
                || $a->{title} cmp $b->{title} }
            grep { any { length > 0 } values %$_ }
            map { $_->{month} = substr($_->{month}, 0, 3); $_ }
            map @{ csv( in => $_, headers => 'auto' ) }, @result_files
        );
    open my $summary, '>data/summary.csv';
    $summary->binmode(':utf8');
    csv in => \@results, out => $summary;
}
