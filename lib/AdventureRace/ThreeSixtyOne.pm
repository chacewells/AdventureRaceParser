package AdventureRace::ThreeSixtyOne;
use namespace::autoclean;
use v5.10;
use Mojo::DOM;

use Moose;

with map "AdventureRace::Role::$_", qw{ Parser Mech };

sub DEFAULT_URL { 'http://361adventures.com/' }

# single event metadata extractors
our $WHEN      = qr/
                    WHEN: \s*
                        (?<WHEN>
                            (?<MONTH>\w+) \s*
                            (?<DAY>\d+ (?:\-\d+)? ) \s*
                            (?:,? \s* (?<YEAR>\d+)?)?
                        )
                 /x;
our $WHERE     = qr/
                    WHERE: \s*
                        (?<WHERE> .*?)
                    \s* ($|[A-Z]+\s*:)
                 /mx; # \n is EOL
our $LOCATION_TRIM
               = qr/
                    ^\s+
                    | (?i:map) \s* $
                    | (?i:locator) \s* $
                    | \s+ $
                 /x;

our $CURRENT_YEAR = do { my ( $yr ) = (localtime)[5]; $yr + 1900 };

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );
    my @urls;
    {
        for my $dropdown ( $dom->find('ul#nav > li')->each ) {
            push @urls, map {
                {
                    url => $_->attr('href'),
                    title => ($_->text =~ s/^\s+|\s+$//gr)
                };
            } $dropdown->find('ul>li.menu-item>a')->each
                if eval { $dropdown->at('a')->text =~ /Races/i };
        }
    }
    # find event pages here
    my @events = map $self->_parse_event_page($_), @urls;

    \@events;
}

sub _parse_event_page {
    no warnings 'experimental';
    my $self = shift;
    my ($url, $title) = @{ shift() }{qw(url title)};
    my $raw = do { $self->mech->get($url); $self->mech->content };
    my $dom = Mojo::DOM->new($raw);
    my %event = (title => $title, website => $url);

    # event info shows up in the form >>WHAT_WE_WANT: value<br/>...<<
    for my $p ( $dom->find('p')->each ) {
        my $text = $p->all_text;

        given ($text) {
            when ($WHEN) {
                no warnings qw(uninitialized numeric);
                $event{month} = $+{MONTH};
                @event{qw(day year)} = map int($+{$_}), qw(DAY YEAR);
                $event{year} ||= $CURRENT_YEAR;
                continue;
            }
            when ($WHERE) {
                $event{location} = $+{WHERE};
                1 while $event{location} =~ s/$LOCATION_TRIM//g; # go til they're all gone
            }
        }
    }

    \%event;
}

__PACKAGE__->meta->make_immutable;
1;
