package AdventureRace::OffroadAR;
use v5.10;

use namespace::autoclean;
use Mojo::DOM;
use Carp qw(croak);
use AdventureRace::Util::RE qw(date_long_re);

use Moose;

with map "AdventureRace::Role::$_", qw'Parser Mech';

my $DATE = date_long_re;

sub DEFAULT_URL { 'http://www.offroadadventureracing.com/' }

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );

    my $race_links = $dom->find('a')->grep(sub { $_->all_text =~ /adventure race/i });
    my @events = $race_links->map($self->_parse_event)->each;

    \@events;
}

sub _parse_event {
    my $self = shift;
    sub {
        my( $title, $url ) = ( $_->all_text =~ s/^\s+|\s+$//gr, $_->{href} );
        $self->mech->get( $url );
        my $dom = Mojo::DOM->new($self->mech->content);
        my %event = ( title => $title, website => $url, map { ($_ => '') } qw(month day year location) );

        for my $div ($dom->find('div')->each) {
            my $text = $div->text;
            next unless $text =~ /\w/;
            if ($text =~ $DATE) {
                $event{lc()} = int(eval{ $+{$_} }) for qw(DAY YEAR);
                $event{month} = eval{ $+{MONTH} } || '';
                last;
            }
        }

        \%event;
    };
}

__PACKAGE__->meta->make_immutable;
1;
