package AdventureRace::BendRacing;
use v5.10;
use namespace::autoclean;
use Mojo::DOM;
use List::Util qw(reduce);

use AdventureRace::Util::RE qw(month_re);

use Moose;

with map "AdventureRace::Role::$_", qw'Parser Mech';

our $MONTH = &month_re;
our $MONTH_AND_DAY = qr/$MONTH \s+ (?<DAY>\d{1,2})?[a-z]*/x;

sub DEFAULT_URL { 'http://bendracing.com/' }

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );
    my $year;
    my @events =
    $dom->find('ul#primary-menu>li')
        ->map(sub {
            if ( $_->at('a')->text =~ /(\d+)\s+races/i ) {
                $year = int $1;
                $_->find('ul.sub-menu>li')->each;
            } else {
                return ();
            }
        })->map( $self->_parse_event($year) )
        ->each;

    \@events;
}

sub _parse_event {
    my($self, $year) = @_;
    sub {
        my $a = $_->at('a');
        my( $url, $title ) = ($a->attr('href'), $a->text);
        $title =~ s/^\s+|\s+$//g;

        my %event = (
            year => $year,
            website => $url,
            title => $title,
            location => ''
        );

        $self->mech->get($url);
        my $dom = Mojo::DOM->new($self->mech->content);
        my $p = $dom->at('div.entry-content>p');
        if ( eval { $p->all_text } =~ $MONTH_AND_DAY ) {
            @event{qw(month day)} = ( $+{MONTH}, int($+{DAY} // 1) );
        }

        \%event;
    };
}

__PACKAGE__->meta->make_immutable;
1;
