package AdventureRace::USARA;
use namespace::autoclean;

use v5.10;
use List::Util qw(reduce);
use Mojo::DOM;

use Moose;

with map "AdventureRace::Role::$_", qw'Parser SimpleUrlGetter';

sub DEFAULT_URL { 'http://www.usara.com/calendar/default.aspx' }

has year => ( is => 'rw', isa => 'Int' );

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new($self->text);

    # USARA's summary page only shows the selected year's events,
    # so extract the year to share with all events
    $self->year(
        reduce { int($a) }
        map { $_->attr('value') }
        grep { $_->text =~ /^\d{4}$/ and defined $_->attr('selected') }
            $dom->find('option')->each
    );

    my @events = map $self->_parse_event, $dom->find('div.calendar-item')->each;

    return \@events;
}

sub _parse_event {
    my $self = shift;
    my $dom = shift // $_;
    my %event;

    @event{qw(month day)} = map $dom->at("div.$_")->text, qw(month day);
    $event{year} = $self->year;

    my @titles = map { $_->text } $dom->find('span.upcomingTitle')->each;
    @event{qw(title location)} = @titles[0,2];
    $event{website} = $dom->at('p.upcomingDesc>a')->attr('href');

    return \%event;
}

1;
