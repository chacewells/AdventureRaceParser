package AdventureRace::CallevaRace;
use v5.10;

use namespace::autoclean;
use Mojo::DOM;
use AdventureRace::Util::RE qw(date_long_re);

use Moose;

my $DATE = date_long_re;
my $DETAILS = qr%
    <strong>When:.*</strong>
        .* $DATE .*
    <strong>Where:.*</strong>
        (?<WHERE>.*) <
%sx;

with map "AdventureRace::Role::$_", qw'Parser Mech';

sub DEFAULT_URL { 'http://callevarace.com/info.html' }

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );

    my @events;
    for my $p ($dom->find('div#schedule p')->each) {
        if ("$p" =~ $DETAILS) {
            push @events, {
                title => "Calleva Race",
                location => $+{WHERE},
                month    => $+{MONTH},
                day      => int(eval {$+{DAY}}) || '',
                year     => int(eval {$+{YEAR}}) || '',
                website  => 'http://callevarace.com/',
            };
        }
    }
    \@events;
}

__PACKAGE__->meta->make_immutable;
1;
