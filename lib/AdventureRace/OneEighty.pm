package AdventureRace::OneEighty;
use namespace::autoclean;
use v5.10;
use Mojo::DOM;
use AdventureRace::Util::RE qw(date_long_re);

use Moose;

with map "AdventureRace::Role::$_", qw{ Parser SimpleUrlGetter };

sub DEFAULT_URL { 'http://www.180adventure.com/calendar/' }

my $DATE = date_long_re;

sub parse_text {
    no warnings 'experimental::smartmatch';
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );

    my @events =
    $dom
    ->find('div.post-entry table tr')
    ->map(sub {
        my $td = $_->at('td');
        return () if $td->at('div');
        my $event = {
            title => '',
            website => &DEFAULT_URL
        };
        do {
            given ($td->text) {
                when ($DATE) {
                    $event->{month} = $+{MONTH};
                    @$event{qw(day year)} = map int, @+{qw(DAY YEAR)};
                }
                when (/,\s*[A-Z]{2}/) {
                    $event->{location} = $td->text;
                }
            }
        } while defined($td = $td->next);

        $event;
    })->each;
    
    \@events;
}

__PACKAGE__->meta->make_immutable;
1;
