package AdventureRace::AXSRacing;
use v5.10;

use namespace::autoclean;
use Mojo::DOM;
use Carp qw(croak);
use AdventureRace::Util::RE qw(month_re);

use Moose;

my $MONTH_RE = month_re;

my $DETAILS_RE = qr/
              Race \s* Date: .*?
              $MONTH_RE .*?
              (?: (?<DAY>\d{1,2}\w{0,2}) \s* , \s*)?
              (?<YEAR>\d{4})
          /x;

with map "AdventureRace::Role::$_", qw'Parser Mech';

sub DEFAULT_URL { 'http://axsracing.com/' }

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );

    my @events;
    $dom->at('div#nav-wrap')
        ->find('li.menu-item')
        ->map(sub {
            return () unless (eval {$_->at('a')->text} // '') =~ /Events/;
            return $_->find('ul.sub-menu>li.menu-item>a')->each;
        })->each(sub {
            return () unless $_->text =~ /[A-Za-z]/;
            my $event = {
                title => eval { $_->text =~ s/\x{2013}/-/gr },
                website => eval { $_->attr('href') },
            };
            push @events, $self->_parse_event($event, $event->{website});
        });

    \@events;
}

sub _parse_event {
    no warnings qw'uninitialized numeric';
    my ( $self, $event, $url, $second_call ) = @_;

    $self->mech->get($url);
    my $dom = Mojo::DOM->new( $self->mech->content );

    my $details_text = $dom->find('div.accordion.toggle')->map(sub {
        return () unless eval { $_->at('h3>a')->text =~ /Race Details/ };
        $_->all_text;
    })->reduce(sub { $a.$b }) // '';

    if ($details_text =~ $DETAILS_RE ) {
        @$event{qw(month day year)} = @+{qw(MONTH DAY YEAR)};
        @$event{qw(day year)} = map 0+$_ || 1, @$event{qw(day year)};
    } elsif ( not $second_call ) {
        if ( my $a = $dom->find('a')->first(sub { $_->all_text =~ /The\s*Race/ }) ) {
            $event = $self->_parse_event( $event, $a->attr('href'), 1 );
        }
    }

    $event;
}

__PACKAGE__->meta->make_immutable;
1;
