package AdventureRace::Role::Parser;
use v5.11;
use namespace::autoclean;
use Carp qw(croak carp);
use File::Slurper qw(read_text);
use Class::CSV;
use YAML ();
use JSON ();

use Moose::Role;
use MooseX::ClassAttribute;

has file => ( is => 'ro',
              isa => 'Str',
              predicate => 'has_file',
              trigger => \&_load_file_text );
has url  => ( is => 'rw',
              isa => 'Str',
              predicate => 'has_url' );
has text => ( is => 'rw',
              isa => 'Str',
              predicate => 'has_text' );
has parsed_output => (
              traits => ['Array'],
              is => 'rw',
              isa => 'ArrayRef[HashRef]',
              predicate => 'has_parsed_output',
              handles => {
                parsed_elems => 'elements'
              });

class_has MONTHS => (
              traits => ['Array'],
              is => 'ro',
              isa => 'ArrayRef[Str]',
              default => sub { [undef, qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)] },
              handles => { get_month => 'get' });

has _csv_output     => ( is => 'rw',
                         isa => 'Str',
                         predicate => 'has_csv_output' );
has _json_output    => ( is => 'rw',
                         isa => 'Str',
                         predicate => 'has_json_output' );
has _yaml_output    => ( is => 'rw',
                         isa => 'Str',
                         predicate => 'has_yaml_output' );
has _line_separator => ( is => 'ro',
                         isa => 'Str',
                         default => "\r\n",
                         init_arg => 'line_separator' );

requires 'parse_text';
requires '_load_url_text';
requires 'DEFAULT_URL';

sub BUILD {
    my $self = shift;
    if ($self->has_url) {
        $self->_load_url_text
    } else {
        $self->url( $self->DEFAULT_URL )
    }
}

sub _load_file_text {
    my ( $self, $file, $old_file ) = @_;

    my $file_text = read_text($file, 'utf8', 'auto') or croak "$!";
    $self->text( $file_text );
}

around parse_text => sub {
    my $orig = shift;
    my $self = shift;
    my $parsed_elems = $self->$orig();
    $self->parsed_output( $parsed_elems );

    return unless defined wantarray;
    wantarray ? @$parsed_elems : $parsed_elems;
};

sub to_csv {
    state $fields = [qw(month day year title location website)];
    my $self = shift;

    $self->parse_text()
        unless $self->has_parsed_output;

    unless ( $self->has_csv_output ) {
        my $csv = Class::CSV->new(
            fields => $fields,
            line_separator => $self->_line_separator,
        );
        
        $csv->add_line({
            month       => $_->{month},
            day         => $_->{day},
            year        => $_->{year},
            title       => $_->{title},
            location    => $_->{location},
            website     => $_->{website},
        }) for $self->parsed_elems;
        $self->_csv_output( $csv->string );
    }

    join(',', @$fields)
    . $self->_line_separator
    . $self->_csv_output;
}

sub to_json {
    my ( $self, @flags ) = @_;

    $self->parse_text()
        unless $self->has_parsed_output;

    unless ( $self->has_json_output ) {
        no warnings 'experimental';
        my $json = JSON->new;
        $json = $json->pretty if @flags and 'pretty' ~~ \@flags;
        my $json_output = $json->encode( $self->parsed_output );
        $self->_json_output( $json_output );
    }

    $self->_json_output;
}

sub to_yaml {
    my $self = shift;

    $self->parse_text()
        unless $self->has_parsed_output;

    unless ( $self->has_yaml_output ) {
        my $yaml = YAML::Dump( $self->parsed_output );
        $self->_yaml_output( $yaml );
    }
    
    $self->_yaml_output;
}

1;
