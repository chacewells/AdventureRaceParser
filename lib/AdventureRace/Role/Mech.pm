package AdventureRace::Role::Mech;
use v5.11;
use namespace::autoclean;

use Moose::Role;
use WWW::Mechanize;

use constant
SPOOF => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36';

has mech => (
    is => 'ro',
    isa => 'WWW::Mechanize',
    default => sub { WWW::Mechanize->new( agent => SPOOF ) }
);

sub _load_url_text {
    my ( $self, $url ) = @_;

    $self->mech->get( $self->url );
    my $url_text = $self->mech->content;
    $self->text( $url_text );
}

1;
