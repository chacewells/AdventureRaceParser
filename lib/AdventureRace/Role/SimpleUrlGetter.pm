package AdventureRace::Role::SimpleUrlGetter;
use v5.11;
use namespace::autoclean;
use LWP::Simple;
use Carp qw(croak);

use Moose::Role;

sub _load_url_text {
    my $self = shift;

    my $lwp_text = get( $self->url ) or croak "Couldn't get text with LWP: $!";
    $lwp_text //= '';
    $self->text( $lwp_text ) if $self->does('AdventureRace::Role::Parser');
}

1;
