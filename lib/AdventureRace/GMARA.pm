package AdventureRace::GMARA;
use v5.10;
use namespace::autoclean;
use Mojo::DOM;
use List::Util qw(reduce);

use AdventureRace::Util::RE qw(date_long_re);

use Moose;

with map "AdventureRace::Role::$_", qw'Parser Mech';

our $DATE = &date_long_re;

sub DEFAULT_URL { 'http://www.gmara.org' }

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );
    my $race_menu = $dom->at('nav>ul li.menu-races ul');
    my @events = $race_menu->find('li')->map( $self->_parse_event )->each;
    
    \@events;
}

sub _parse_event {
    my $self = shift;
    sub {
        return () if eval { $_->attr('class') } =~ /overview|race-results/;
        my $a = $_->at('a');
        my %event = (
            website => $self->DEFAULT_URL . $a->attr('href'),
            title => $a->text,
        );
        s/^\s+|\s+$//g for values %event;

        $self->mech->get( $event{website} );
        my $dom = Mojo::DOM->new($self->mech->content);
        foreach my $p ($dom
           ->at('h2#introduction')
           ->parent->find('p')->each) {
            if (eval { $p->all_text } =~ $DATE) {
                @event{qw(month day year)} = @+{qw(MONTH DAY YEAR)};
                do { $_ = int } for @event{qw(day year)};
                last;
            }
        }

        foreach my $p ($dom
            ->at('h2#directions')
            ->parent->find('p')->each) {
                if ($p->text =~ /Our host venue will be/) {
                    $event{location} = eval { $p->at('a')->text } // '';
                    $event{location} =~ s/^\s+|\s+$//g;
                    last;
                }
        }

        \%event;
    };
}

__PACKAGE__->meta->make_immutable;
1;
