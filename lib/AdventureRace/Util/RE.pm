package AdventureRace::Util::RE;
use strict;
use Exporter qw(import);

our @EXPORT_OK = qw(
    month_re
    day_of_month_re
    year_re
    date_long_re
    month_day
);

sub month_re {
    qr/ (?<MONTH>
            Jan\.?(?:uary)?
          | Feb\.?(?:ruary)?
          | Mar\.?(?:ch)?
          | Apr\.?(?:il)?
          | May
          | June?
          | July?
          | Aug\.?(?:ust)?
          | Sept\.?(?:ember)?
          | Oct\.?(?:ober)?
          | Nov\.?(?:ember)?
          | Dec\.?(?:ember)?
         )/x
}

sub day_of_month_re {
    qr/(?<DAY>\d{1,2})(?i:st|th|nd|rd)?/
}

sub year_re {
    qr/(?<YEAR>\d{4})/
}

sub date_long_re {
    my( $month, $day, $year ) = (&month_re, &day_of_month_re, &year_re);
    qr/
        (?<DATE>
            $month \s* ,? \s*
            $day \s* ,? \s*
            $year
        )
    /x;
}

sub month_day {
    my( $month, $day, $year ) = (&month_re, &day_of_month_re, &year_re);
    qr/
        (?<DATE>
            $month \s* ,? \s*
            $day
        )
    /x;
}

1;
