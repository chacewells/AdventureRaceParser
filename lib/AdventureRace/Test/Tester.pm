package AdventureRace::Test::Tester;
use namespace::autoclean;
use Moose;
use strict;
use feature qw(say);

use JSON qw( from_json );

with qw( AdventureRace::Role::SimpleUrlGetter AdventureRace::Role::Parser );

sub DEFAULT_URL { 'http://www.wellsfromwales.com/' }

sub parse_text {
    my $text = shift->text;
    from_json $text;
}

__PACKAGE__->meta->make_immutable;
1;
