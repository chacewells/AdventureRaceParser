package AdventureRace::GoalsARA;
use v5.11;
use namespace::autoclean;
use Mojo::DOM;
use List::Util qw(reduce);

use Moose;

with map "AdventureRace::Role::$_", qw*Parser Mech*;

sub DEFAULT_URL { 'http://goalsara.org/' }

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );
    my $events_div = $dom->at( 'div.tribe-events-loop' );
    my @events = $events_div->children->each->map(\&_parse_event);
    \@events;
}

sub _parse_event {
    state $year;

    my $elem = $_;
    my %event;

    if ( $elem->matches('span.tribe-events-list-separator-month') ) {
        $year = int( (split /\s/, $elem->at('span')->text)[1] );
        return () if $year;
    }

    if ( $elem->matches('div.type-tribe_events') ) {
        $event{year} = $year;
        if (my $ta = eval { $elem->at('h2.tribe-events-list-event-title a.tribe-event-url') }) {
            @event{'title','website'} = ($ta->text, $ta->attr->{href});
        }
        @event{'month','day'} = split /[,\s]+/, $elem->at('span.tribe-event-date-start')->text;
        $event{day} = int $event{day};
        $event{location} = $elem->find('div.tribe-events-venue-details span')
                                ->map(sub { $_->text })
                                ->map(sub { s/^[\s,]+|[,\s]+$//gr || () })
                                ->reduce(sub { "$a, $b" });
    }
    \%event;
};

__PACKAGE__->meta->make_immutable;
1;

__END__
=head1 some ideas to toss around for making Goals ARA more flexible
The Goals ARA path spec seems to be much more predictable and stable than its naming for classes
so simply gather all 'a' tags, map to href, then filter by ones that look like future races
=cut
use WWW::Mechanize;
use Mojo::DOM;
use List::MoreUtils qw(uniq);

use constant
SPOOF => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36';

my $mech = WWW::Mechanize->new( agent => SPOOF );
$mech->get('http://goalsara.org/');

my $dom = Mojo::DOM->new($mech->content);

for my $a (reverse uniq grep &future_race, $dom->find('a')->map(sub { $_->{href} })->each) {
    say $a;
}

sub future_race {
    state $nowyear = (localtime)[5] + 1900;
    m@ (\d{4}) -races / [^/]+? / $@x && $1 >= $nowyear;
}
