package AdventureRace::NCARS;
use v5.10;

use namespace::autoclean;
use Mojo::DOM;
use Carp qw(croak);
use AdventureRace::Util::RE qw(month_day year_re);

use Moose;

with map "AdventureRace::Role::$_", qw'Parser Mech';

my $MONTH_DAY = month_day;
my $YEAR = year_re;

sub DEFAULT_URL { 'http://ehfcapital.com/www.ncars.info/' }

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );

    my $race_menu =
    $dom->at('nav.menu-container>ul#menu')
        ->descendant_nodes
        ->grep(type => 'a')
        ->grep(sub { $_->text =~ qr/races/i })
        ->first->next;
    my @events = $race_menu->find('a')->map($self->_parse_event)->each;

    \@events;
}

sub _parse_event {
    my $self = shift;

    sub {
        my %event = (
            title => (eval { $_->text =~ s/^\s+|\s+$//gr }),
            website => $self->DEFAULT_URL . $_->{href},
        );
        $self->mech->get($event{website});

        my $dom = Mojo::DOM->new($self->mech->content);
        if ($dom->at('h1')->text =~ $YEAR) {
            $event{year} = $+{YEAR};
        }

        my $p = eval {
        $dom->find('p')
            ->grep(sub {
                $_->all_text =~ /check\W*in \s+ date|race \s+ start:/xi
            })->first };
        my $text = eval { "$p" } // '';
        if ($text =~ $MONTH_DAY) {
            @event{qw(month day)} = ($+{MONTH}, int $+{DAY});
        }
        if ($text =~ m%
                (?:hq|trails \s* near) .*? : \s* </strong>
                (.*?)
                <
            %gxi) {
            $event{location} = $1;
            $event{location} =~ s/^\s+|\s+$//g;
        }

        \%event;
    };
}

__PACKAGE__->meta->make_immutable;
1;
