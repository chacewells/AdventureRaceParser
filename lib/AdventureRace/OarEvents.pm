package AdventureRace::OarEvents;
use namespace::autoclean;
use v5.10;
use Mojo::DOM;

use Moose;

with map "AdventureRace::Role::$_", qw{ Parser SimpleUrlGetter };

sub DEFAULT_URL { 'http://www.oarevents.com/' }

has year => ( is => 'rw', isa => 'Int' );

our $DATE = qr/
                (?<MONTH>[A-Z][a-z]{2,3})\.?
                \s*?
                \b (?<DAY> \d{1,2}) \b
            /x;

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );

    my @events = $dom->find('article.uk-article>h1, article.uk-article>p')
        ->map($self->_parse_event)->each;
    \@events;
}

sub _parse_event {
    my $self = shift;

    return sub {
        if ( $_->tag eq 'h1' ) {
            $self->year( $1 )
                if $_->all_text =~ /(\d{4})/;
            return ();
        }
        my $text = $_->all_text;
        return () unless $text =~ /$DATE/;
        my($month, $day) = ( $+{MONTH}, int($+{DAY}) );
        my $a = $_->at('a');
        my($title, $website) = ($a->text, $a->attr('href'));
        
        $title =~ s/^\s+|\s+$//g;
        $website = &DEFAULT_URL . substr($website, 1)
            if $website =~ m#^/#;

        my $location = do { $1 if $text =~ /(\b[A-Z][a-z]+\b,\s+[A-Z]{2})/ };

        return {
            month => $month,
            day => $day,
            year => $self->year,
            title => $title,
            location => $location,
            website => $website,
        };
    };
}
