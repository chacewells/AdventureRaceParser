package AdventureRace::Krank;
use namespace::autoclean;
use v5.10;
use Mojo::DOM;
use LWP::Simple;

use Moose;

with map "AdventureRace::Role::$_", qw'Parser SimpleUrlGetter';

sub DEFAULT_URL { 'http://www.krankevents.com' }

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new($self->text);
    my @urls = map { &DEFAULT_URL . $_->attr('href') } $dom->find('a.eb_event_link')->each;
    my @events = map $self->_parse_event_page($_), @urls;

    \@events;
}

sub _parse_event_page {
    my $self = shift;
    my $url = shift;
    my $raw = get $url;
    my $dom = Mojo::DOM->new($raw);
    my %event;

    $event{website} = $url;

    # parse title from $('div#eb_docs h1.eb_title')[0]
    $event{title} = $dom->at('div#eb_docs h1.eb_title')->text =~ s/^\s+|\s+$//gr;
    
    # find location url (should be in 'div#eb_details>div#detail_left>table td' (wherever "Location" shows up))
    my $location_url;
    for my $tr ( $dom->find('tr')->each ) {
        # parse year, month, day from "Event Date:" line
        $self->_parse_event_date_if_present(\%event, $tr);
    
        my ( $label, $data ) = eval { $tr->find('td')->each };
        $location_url = &DEFAULT_URL . eval { $data->at('a')->attr('href') =~ s/^\s+|\s+$//gr }
                                    if eval { $label->content =~ /Location/ };
    }
    # &_retrieve_location_details
    if ($location_url) {
        $self->_retrieve_location_details(\%event, $location_url);
    }
    $event{location} = 'TBD' unless $event{location};

    # return in hash ref
    return \%event;
}

sub _parse_event_date_if_present {
    my $self = shift;
    my $event = shift;
    my $tr = shift // $_;

    my ( $label, $data ) = $tr->find('td')->each;
    if (eval { $label->at('strong')->text =~ /Event Date:/ }) {
        my($m,$d,$y) = $data->text =~ /(\d\d)-(\d\d)-(\d\d\d\d)/;
        @$event{qw(month day year)} = map int, ($m,$d,$y);

        # just in case we don't actually have a date
        $event->{$_} //= 1 for qw(month day year);
        $event->{month} = __PACKAGE__->get_month($event->{month}) // 'Jan';
    }
}

sub _retrieve_location_details {
    my $self = shift;
    my $event = shift;
    my $url = shift // $_;

    # retrieve map text
    my $page_text = get $url;

    # get text of contentString variable
    my ( $contents ) = $page_text =~ m{var\s+contentString\s=\s'(.*?)'};
    my $loc_dom = Mojo::DOM->new($contents);

    # parse location details (name, address, city, state) into hash
    my $venue = eval { $loc_dom->at('li.location_name>h4')->text =~ s/^\s+|\s+$//gr };
    my $address = eval { $loc_dom->at('li.address')->text =~ s/^\s+|\s+$//gr };
    $event->{location} = "$venue, $address";
}

1;
