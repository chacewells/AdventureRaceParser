package AdventureRace::AdventureRacingNet;
use namespace::autoclean;
use v5.10;
use Mojo::DOM;
use AdventureRace::Util::RE qw(date_long_re);
use Carp qw(croak);

BEGIN {
    our $VERSION = 1.0;
}

use Moose;

with map "AdventureRace::Role::$_", qw{ Parser Mech };

sub DEFAULT_URL { 'http://adventureracing.net/' }

my $DATE = date_long_re;

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );
    my $found_events_page;

    # go to events page
    for ($dom->find("a")->each) {
        if (eval { $_->text =~ /events/i } and exists($_->{href})) {
            my $url = $self->DEFAULT_URL.$_->{href};
            $self->mech->get($url);
            $dom = Mojo::DOM->new($self->mech->content);
            $found_events_page = 1;
            last;
        }
    }
    croak "Could not find events page: $!" unless $found_events_page;

    # gather basic event info
    my @events;
    my $crawler = _event_div_crawler($dom);
    while (defined(my $div = $crawler->())) {
        my %event;
        $event{title} = eval { $div->at("span.hd04")->text } =~ s/^\s+|\s+$//gr;
        my $date_text = eval { $div->at('span.sch_date')->text };
        if ( $date_text =~ /$DATE/ ) {
            @event{qw(day year)} = map int($_ // 0), @+{qw(DAY YEAR)};
            $event{month} = $+{MONTH};
        }
        for my $a ($div->find('a')->each) {
            last unless $a->text =~ /Information/;
            $event{website} = DEFAULT_URL . $a->{href};
            last;
        }
        push @events, \%event;
    }
    # visit each page to gather details
    $self->_append_location($_) for @events;
    \@events;
}

sub _append_location {
    my ($self, $event) = @_;
    $self->mech->get($event->{website});
    my $dom = Mojo::DOM->new($self->mech->content);
    my $info_node = $dom->at("div.page_sec01")->children->first;
    while (defined $info_node) {
        next unless $info_node->text =~ /location/i;
        my $loc_node = $info_node->next;
        my $location = $loc_node->content;
        for ($location) {
            s%<br/?>%, %g;
            s/\n+//g;
            s/^\s+|\s+$//g;
            s/\s+/ /g;
            s/,$//;
        }
        $event->{location} = $location;
    } continue {
        $info_node = $info_node->next;
    }
}

sub _event_div_crawler {
    my $dom = shift;
    my $event_number = 1;
    return sub {
        my $div = eval { $dom->at("div#ContentPlaceHolder1_Event$event_number") };
        ++$event_number;
        $div;
    };
}

__PACKAGE__->meta->make_immutable;
1;
