package AdventureRace::WorldTeamSports;
use v5.10;
use namespace::autoclean;
use Mojo::DOM;
use List::Util qw(reduce);
use LWP::Simple;

use Moose;

with map "AdventureRace::Role::$_", qw'Parser SimpleUrlGetter';

sub DEFAULT_URL { 'http://worldteamsports.org/events/' }

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );
    my @events =
    $dom->at('div#main_menu')
        ->find('li')
        ->map(sub {
            return () unless $_->at('a')->text eq 'Events';
            $_->find('li.menu-item')->each;
        })->map($self->_parse_event);

    \@events;
}

sub _parse_event {
    my $self = shift;
    sub {
        my $li = $_;
        my $a = $li->at('a');
        my ( $title, $url ) = map s/^\s+|\s+$//gr, $a->text, $a->attr('href');
        my $event = { title => $title, website => $url };
        my $dom = Mojo::DOM->new( get($url) );
    };
}
