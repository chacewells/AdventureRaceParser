package AdventureRace::MichiganAdventureRace;
use v5.10;
use namespace::autoclean;
use Mojo::DOM;
use AdventureRace::Util::RE qw(date_long_re);

use Moose;

with map "AdventureRace::Role::$_", qw*Parser Mech*;

sub DEFAULT_URL { 'http://www.miadventurerace.com/' }

my $DATE = &date_long_re;

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );

    my @events = $dom
        ->find('ul#primary_nav > li.menu-item')
        ->map($self->_parse_event)->each;
    \@events;
}

sub _parse_event {
    my $self = shift;
    sub {
        my $menu_item = $_;
        my $title = eval { $menu_item->at('a')->text =~ s/^\s+|\s+$//gr }
            or return ();
        my $website = eval {
            $menu_item
            ->at('ul.sub-menu>li>a')
            ->attr('href') }
                or return ();
        $self->mech->get( $website );
        my $page = Mojo::DOM->new( $self->mech->content );
        # get race details here

        my $event = { title => $title, website => $website };
        $page->find('table.tablepress tr')->each(sub {
            no warnings 'experimental::smartmatch';
            my($col1, $col2) = $_->find('td')->each;
            given ($col1->text) {
                when (/Date:/i) {
                    if ( $col2->text =~ $DATE ) {
                        local $" = ", ";
                        $event->{month} = $+{MONTH};
                        @$event{qw(day year)} = map int($+{$_}), qw(DAY YEAR);
                    }
                }
                when (/Location:/i) {
                    $event->{location} = $col2->text;
                }
            }
        });

        my $event_text = $page->at('div.entry-content > h2')->content;

        # find location if it's not there
        unless ( exists $event->{location} ) {
            my @data = split /<br>/, $event_text;
            $event->{location} = ($data[1] =~ s/^\s+|\s+$//gr) . ', MI';
        }

        # find the dates if they're not there
        unless ( exists $event->{month} ) {
            $event_text =~ s%<img.*?>%%g;
            if ( $event_text =~ $DATE ) {
                $event->{month} = $+{MONTH};
                @$event{qw(day year)} = map int, @+{qw(DAY YEAR)};
            }
        }
        $event;
    };
}

__PACKAGE__->meta->make_immutable;
1;
