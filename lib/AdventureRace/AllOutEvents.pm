package AdventureRace::AllOutEvents;
use namespace::autoclean;
use v5.10;
use utf8;
use Mojo::DOM;
use AdventureRace::Util::RE qw(date_long_re);

use Moose;

with map "AdventureRace::Role::$_", qw{ Parser Mech };

sub DEFAULT_URL { 'http://all-outevents.com/events/' }

my $DATE = date_long_re;

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );

    my @events;
    for ( my $e = $dom->at('div.entry-content')->children->first; defined $e; $e = $e->next ) {
        state $ar_section = 0;
        $ar_section = 1 if $e->tag eq 'p' and $e->all_text =~ /Adventure Races/;
        $ar_section = 0 if $ar_section and $e->tag eq 'p' and $e->all_text !~ /Adventure Races/;

        if ($ar_section and $e->tag eq 'ul') {
            my $li = $e->at('li');

            if ($li->text =~ /^$DATE.*[-\x{2013}](?<location>.*?)$/) {
                my %event;
                $event{lc($_)} = (defined $+{$_} ? int( $+{$_} ) : '') for qw(DAY YEAR);
                $event{month} = $+{MONTH};
                $event{location} = $+{location};
                my $a = $li->at('a');
                @event{qw(title website)} = defined $a ? ($a->all_text, $a->{href}) : ('') x 2;
                s/^\s+|\s+$//g for @event{qw(location title website)};
                push @events, \%event;
            } else { next }
        }
    }
    \@events;
}

__PACKAGE__->meta->make_immutable;
1;
