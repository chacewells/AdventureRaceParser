package AdventureRace::HappyMutant;
use v5.11;
use namespace::autoclean;
use Mojo::DOM;
use List::Util qw(reduce);

use Moose;

with map "AdventureRace::Role::$_", qw'Parser SimpleUrlGetter';

sub DEFAULT_URL { 'http://www.thehappymutant.com/events/' }

sub parse_text {
    my $self = shift;
    my $dom = Mojo::DOM->new( $self->text );
    my $events_div = $dom->at( 'div.tribe-events-loop' );
    [map &parse_event, $events_div->children->each];
}

sub parse_event {
    state $year;

    my $elem = $_;
    my %event;

    if ( $elem->matches('span.tribe-events-list-separator-month') ) {
        $year = int( (split /\s/, $elem->at('span')->text)[1] );
        return () if $year;
    }

    if ( $elem->matches('div.type-tribe_events') ) {
        $event{year} = $year;
        if (my $ta = eval { $elem->at('h2.tribe-events-list-event-title a.tribe-event-url') }) {
            @event{'title','website'} = ($ta->text, $ta->attr->{href});
        }
        @event{'month','day'} = split /[,\s]+/, $elem->at('span.tribe-event-date-start')->text;
        $event{day} = int $event{day};
        $event{location} = reduce { "$a, $b" }
                                map { s/^[\s,]+|[,\s]+$//gr || () }
                                map { $_->text }
                                $elem->find('div.tribe-events-venue-details span')->each();
    }
    \%event;
}

__PACKAGE__->meta->make_immutable;
1;
