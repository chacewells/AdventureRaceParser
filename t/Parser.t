use v5.11;
use Test::More;
use AdventureRace::Test::Tester;
use File::Slurper qw/read_text write_text/;
use JSON;
use Data::Compare;
use YAML ();

use constant SOURCE_JSON => 't/source_json.json';
use constant EXPECTED_CSV => 't/expected_csv.csv';
use constant EXPECTED_YAML => 't/expected_yaml.yml';

{ # Test basic text
    my $test_string = 'hello tester!';
    my $obj = AdventureRace::Test::Tester->new(text => $test_string);
    is( $obj->text, $test_string, 'should give back its text');
}

{ # Test file
    my $tmpfile = 'temp.txt';
    write_text($tmpfile, my $expected = "i want this text back\n");
    my $p = AdventureRace::Test::Tester->new(file => $tmpfile);
    is( $p->text, $expected, "should get back the tmpfile text" );
    unlink $tmpfile;
}

{ # Test URL
    my $url = "http://www.wellsfromwales.com/";
    my $expected_first_line = qr/!doctype html/i;
    my $p = AdventureRace::Test::Tester->new( url => $url );
    my ( $first_line ) = split "\n", $p->text;
    like( $first_line, $expected_first_line, "first line should be a DOCTYPE declaration" );
}

{ # Test general parsing business
    my($year, $month, $day) = (2016, 'Jan', 21);
    my($title, $location, $website) =
      ("Tron", "Trent, IL", "www.foo.bar");
    my $json_text = read_text SOURCE_JSON;
    my $parser = AdventureRace::Test::Tester->new(text => $json_text);
    my $output = $parser->parse_text;
    is( $output->[0]{month}, $month, "months match" );
    is( $output->[0]{day}, $day, "days match" );
    is( $output->[0]{year}, $year, "years match" );
    is( $output->[0]{location}, $location, "locations match" );
    is( $output->[0]{title}, $title, "titles match" );
    is( $output->[0]{website}, $website, "websites match" );
}

{ # try out the CSV
    my $json_text = read_text SOURCE_JSON;
    my $expected = read_text EXPECTED_CSV;
    my $p = AdventureRace::Test::Tester->new(text => $json_text, line_separator => "\n");
    is( $p->to_csv, $expected, "should be the same CSV" );
}

{ # try out the JSON
    my $p = AdventureRace::Test::Tester->new(file => SOURCE_JSON);
    my $expected = JSON->new->decode(read_text SOURCE_JSON);
    my $actual = JSON->new->decode( $p->to_json );
    my $comp = Data::Compare->new($expected, $actual);
    ok( $comp->Cmp, 'should be the same JSON' );
}

{ # try out the YAML
    my $p = AdventureRace::Test::Tester->new(file => SOURCE_JSON);
    my $expected = YAML::Load(read_text EXPECTED_YAML);
    my $actual = YAML::Load( $p->to_yaml );
    my $comp = Data::Compare->new($expected, $actual);
    ok( $comp->Cmp, 'should be the same YAML' );
}

done_testing();
